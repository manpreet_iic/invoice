<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Organisation;
use app\models\Product;
use dosamigos\datepicker\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
/* @var $model app\models\InvoiceItem */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoice-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'orgid')->dropDownList(
                ArrayHelper::map(Organisation::find()->all(),'orgid','orgname'),
                ['prompt'=>'Select organisation']
            ) ?>
        </div>
        <div class="col-md-6">
    <?= $form->field($model, 'invno')->textInput() ?>
        </div>
    </div>
    <div class="row">
    
        <div class="col-sm-6">
        <div class=”col-sm-6">
            <?= $form->field($model, 'date')->widget(
                DatePicker::className(),[
                'inline'=>false,
                //  'template' =>'<div class="well well-sm" style="background-color:#fff;width:250px">{input}</div>',
                'clientOptions'=>[
                    'autoclose' =>true,
                    'format'=>'yyyy-mm-dd'
                ]
            ]);?>

        </div>
        </div>
    </div>
        <div class="row">
        <div class="col-sm-6">
        <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
