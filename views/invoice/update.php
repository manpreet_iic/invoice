<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Product;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Invoice */

$this->title = Yii::t('app', 'Update Invoice: {name}', [
    'name' => $model->invid,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Invoices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->invid, 'url' => ['view', 'id' => $model->invid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="invoice-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-sm-3">
              <?= $form->field($item,'productid')->dropDownList(
                ArrayHelper::map(Product::find()->all(),'productid','prodname'),
                   ['prompt'=>'Select Item']
                ) ?>
            </div>
              <div class="col-sm-3">
                    <?= $form->field($product, 'proddesc')->textInput(['maxlength' => true]) ?>
             </div>
             <div class="col-sm-3">
                <?= $form->field($item, 'quanty')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($item, 'unitcost')->textInput(['maxlength' => true]) ?>
            </div>
    </div><!-- .row -->
   

    <div class="row">
        <div class="col-sm-6">
        <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
    'dataProvider' => $dataProvider,

    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
       'product.prodname',
    
    ],
]); ?>
    
    
</div>
