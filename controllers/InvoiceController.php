<?php

namespace app\controllers;

use app\models\InvoiceItem;
use app\models\Product;
use app\models\Model;
use Yii;
use app\models\Invoice;
use app\models\InvoiceSearch;
use app\models\InvoiceItSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\data\ArrayDataProvider;

/**
 * InvoiceController implements the CRUD actions for Invoice model.
 */
class InvoiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
           'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoice model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Invoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Invoice();
    
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
           return $this->redirect(['view', 'id' => $model->invid]);
        }
else
{
        return $this->render('create', [
            'model' => $model,
        ]);
}
    
    }

    /**
     * Updates an existing Invoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @parsam integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $value = $this->fModel($id);
        $dataProvider = new ArrayDataProvider([
           'allModels'=>$value,
        ]);
    
        $product = new Product();
        $item= new InvoiceItem();

        
        if ($product->load(Yii::$app->request->post()) && $item->load(Yii::$app->request->post()))
        {    
     
       $item->invid=$id;
       $item->save();
       return $this->render('update', [
        'model' => $model,'product'=>$product,'item'=>$item,'dataProvider'=>$dataProvider,
     ]);
       }  
          return $this->render('update', [
        'model' => $model,'product'=>$product,'item'=>$item,'dataProvider'=>$dataProvider,
     ]);
    }

    /**
     * Deletes an existing Invoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    protected function fModel($id)
    {
        print_r($id);  
        $value = InvoiceItem::findAll(['invid'=>$id]);
        return $value;
    
    }
}
