<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisation".
 *
 * @property int $orgid
 * @property string|null $orgname
 * @property string|null $orgaddress
 * @property string|null $phoneno
 *
 * @property Invoice[] $invoices
 */
class Organisation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organisation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orgname'], 'string', 'max' => 60],
            [['orgaddress'], 'string', 'max' => 150],
            [['phoneno'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'orgid' => 'Organisation ID',
            'orgname' => 'Organsiation Name',
            'orgaddress' => 'Address',
            'phoneno' => 'Phoneno',
        ];
    }

    /**
     * Gets query for [[Invoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['orgid' => 'orgid']);
    }
}
