<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $productid
 * @property string|null $prodname
 * @property string|null $proddesc
 * @property float|null $unitprice
 *
 * @property Invoiceitem[] $invoiceitems
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unitprice'], 'number'],
            [['prodname'], 'string', 'max' => 60],
            [['proddesc'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'productid' => 'Productid',
            'prodname' => 'Prodname',
            'proddesc' => 'Proddesc',
            'unitprice' => 'Unitprice',
        ];
    }

    /**
     * Gets query for [[Invoiceitems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceitems()
    {
        return $this->hasMany(Invoiceitem::className(), ['productid' => 'productid']);
    }
}
