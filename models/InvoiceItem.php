<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "invoiceitem".
 *
 * @property int $id
 * @property int $productid
 * @property int $invid
 * @property int $unitcost
 * @property int $quanty
 *
 * @property Invoice $inv
 * @property Product $product
 */
class InvoiceItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoiceitem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['productid', 'invid', 'unitcost', 'quanty'], 'required'],
            [['productid', 'invid', 'unitcost', 'quanty'], 'integer'],
            [['invid'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invid' => 'invid']],
            [['productid'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productid' => 'productid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productid' => 'Productid',
            'product.prodname' =>'Product Name',
            'product.proddesc' =>'Product Description',
            'invid' => 'Invid',
            'unitcost' => 'Unitcost',
            'quanty' => 'Quantity',
        ];
    }

    /**
     * Gets query for [[Inv]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInv()
    {
        return $this->hasOne(Invoice::className(), ['invid' => 'invid']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['productid' => 'productid']);
    }
}
