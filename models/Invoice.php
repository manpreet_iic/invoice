<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "invoice".
 *
 * @property int $invid
 * @property int|null $invno
 * @property int $orgid
 * @property float|null $amtdue
 * @property float|null $subtotal
 * @property float|null $total
 * @property string $date
 *
 * @property Organisation $org
 * @property Invoiceitem[] $invoiceitems
 */
class Invoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invno', 'orgid'], 'integer'],
            [['invno','orgid','date'], 'required'],
            [['amtdue', 'subtotal', 'total'], 'number'],
            [['date'], 'safe'],
            ['date','checkDate'],
            [['orgid'], 'exist', 'skipOnError' => true, 'targetClass' => Organisation::className(), 'targetAttribute' => ['orgid' => 'orgid']],
        ];
    }

    public function checkDate($attribute,$params){
        $today=date('yyyy-mm-dd');
        $selectdate=date($this->date);
        if($selectdate > $today)
        {
            $this->addError($attribute,'Date should not be greater than Today');
        }

    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'invid' => 'ID',
            'invno' => 'Invoice No',
            'orgid' => 'Organisation Name',
            'org.orgname' => 'Organisation Name',
            'amtdue' => 'Amount due',
            'subtotal' => 'Subtotal',
            'total' => 'Total',
            'date' => 'Date',
        ];
    }

    /**
     * Gets query for [[Org]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(Organisation::className(), ['orgid' => 'orgid']);
    }

    /**
     * Gets query for [[Invoiceitems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceitems()
    {
        return $this->hasMany(Invoiceitem::className(), ['invid' => 'invid']);
    }
}
